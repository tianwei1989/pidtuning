import re
import os
import subprocess
import math
import shutil

##run the script in ../CASE5/FFD/figure1.mcr
##subprocess.call(('tec360','../CASE5/FFD/figure1.mcr'))

##Read the files U1 to U10
##and copy them to current folder with revisions and extractions

ls=[1,2,3,4,5,6,7,8,9,10]
datafile=["U"+str(i)+".dat" for i in ls ]
#datafile=["U1.dat"]
print datafile


for i in datafile:
    Z=[]
    U=[]
    V=[]
    W=[]
    Vel=[]
    T=[]

    
    #if there are files existed, delete them
    if os.path.exists(i):
        print "will delete file %s" % i
        os.remove(i)

        
    #open the extracted data file
    with open('../'+i,'r+') as myfile:
        for index, line in enumerate (myfile):
            if index < 16:
                continue
            else:
                Z.append(line.split()[2])
                Vel.append(line.split()[9])
                T.append(line.split()[10])
                
    #conventions
    for index, n in enumerate(Z):
        Z[index] = float(n)/2.44
        
    for index, n in enumerate(Vel):
        Vel[index] = float(n)/1.5
        
    for index, n in enumerate(T):
        T[index] = (float(n)-22.2)/(36.7-22.2)

        
    #write the new velocity file
    with open (i, 'w') as myfile:
            myfile.writelines(
'''\
TITLE="DATA FOR case3 POINT %s" 
VARIABLES="U/Umax" "z/L"
ZONE T="%s"
''' % (i[1],(i[0]+i[1]))
    )
            for index, l in enumerate (Vel):
                myfile.writelines("%f\t%f\n" %(float(l), float(Z[index])))


    #write the new temperature file
    if i=="U10.dat":
        name="T10.dat"
    else:
        name="T"+i[1]+".dat"
        
    with open (name, 'w') as myfile:
            myfile.writelines(
'''\
TITLE="DATA FOR case3 POINT %s" 
VARIABLES="Tstar" "z/L"
ZONE T="T %s"
''' % (i[1],(i[0]+i[1]))
    )
            for index, l in enumerate (T):
                myfile.writelines("%f\t%f\n" %(float(l), float(Z[index])))
           

##plotting

                
dic=dict(U1="case3_velocity1.lpk", U3="case3_velocity3.lpk", U5="case3_velocity5.lpk", U6="case3_velocity6.lpk")
draw=["U1", "U3", "U5", "U6"]
for i in draw:
    if os.path.exists(dic[i]):
        print "%s exists" %dic[i]
        os.remove(dic[i])
    shutil.copy ("../Experiment/Miao data/"+dic[i],"./")
    
dic=dict(T1="case3_temperature1.lpk", T3="case3_temperature3.lpk", T5="case3_temperature5.lpk", T6="case3_temperature6.lpk")
draw=["T1", "T3", "T5", "T6"]
for i in draw:
    if os.path.exists(dic[i]):
        print "%s exists" %dic[i]
        os.remove(dic[i])
    shutil.copy ("../Experiment/Miao data/"+dic[i],"./")


#copy the original file and rename accordingly
dic=dict(U1="case3_velocity1.mcr", U3="case3_velocity3.mcr", U5="case3_velocity5.mcr", U6="case3_velocity6.mcr")
draw=["U1", "U3", "U5", "U6"]
for i in draw:
    if os.path.exists(dic[i]):
        print "%s exists" %dic[i]
        os.remove(dic[i])
    try:
        with open(dic[i],'a+') as f:
            pass
    except KeyError as e:
        print e
    shutil.copyfile ("process.mcr", dic[i])
    
dic=dict(T1="case3_temperature1.mcr", T3="case3_temperature3.mcr", T5="case3_temperature5.mcr", T6="case3_temperature6.mcr")
draw=["T1", "T3", "T5", "T6"]
for i in draw:
    if os.path.exists(dic[i]):
        print "%s exists" %dic[i]
        os.remove(dic[i])
    try:
        with open(dic[i],'a+') as f:
            pass
    except KeyError as e:
        print e
    shutil.copyfile ("processT.mcr", dic[i])


#edit mcr file accordingly
mcrdic=dict(U1="case3_velocity1.mcr", U3="case3_velocity3.mcr", U5="case3_velocity5.mcr", U6="case3_velocity6.mcr")
lpkdic=dict(U1="case3_velocity1.lpk", U3="case3_velocity3.lpk", U5="case3_velocity5.lpk", U6="case3_velocity6.lpk")
draw=["U1", "U3", "U5", "U6"]
for i in draw:
    lines=[]
    #open file
    with open (mcrdic[i],"r+") as myfile:
        lines=myfile.readlines()
        print type(lines)
    with open (mcrdic[i],"w") as myfile:
        for index, line in enumerate (lines):
            ##Note that can't directly assign replaced value to line
            ##because line is a string which is not mutable
            if ("case3_velocity1.lpk" in line):
                print line + 'will be replaced'
                lines[index] = line.replace("case3_velocity1.lpk",lpkdic[i])
            elif ("U1.dat" in line):
                print line + 'will be replaced'
                lines[index]=line.replace("U1.dat", "%s.dat" % i)
            elif ("U1.eps" in line):
                #lines[index]=line.replace("U1.eps","NEW_%s.eps" % i)
				lines[index]=line.replace("U1.eps","Figure2_%s.eps" % i[1])
            elif ("U1.png" in line):
                #lines[index]=line.replace("U1.png","NEW_%s.png" % i)
				lines[index]=line.replace("U1.png","Figure2_%s.png" % i[1])
            elif ("U1.jpeg" in line):
                #lines[index]=line.replace("U1.jpeg","NEW_%s.png" % i)
				lines[index]=line.replace("U1.jpeg","Figure2_%s.jpeg" % i[1])
        myfile.writelines(lines)

mcrdic=dict(T1="case3_temperature1.mcr", T3="case3_temperature3.mcr", T5="case3_temperature5.mcr", T6="case3_temperature6.mcr")
lpkdic=dict(T1="case3_temperature1.lpk", T3="case3_temperature3.lpk", T5="case3_temperature5.lpk", T6="case3_temperature6.lpk")
draw=["T1", "T3", "T5", "T6"]
for i in draw:
    lines=[]
    #open file
    with open (mcrdic[i],"r+") as myfile:
        lines=myfile.readlines()
        print type(lines)
    with open (mcrdic[i],"w") as myfile:
        for index, line in enumerate (lines):
            ##Note that can't directly assign replaced value to line
            ##because line is a string which is not mutable
            if ("case3_temperature1.lpk" in line):
                print line + 'will be replaced'
                lines[index] = line.replace("case3_temperature1.lpk",lpkdic[i])
            elif ("T1.dat" in line):
                print line + 'will be replaced'
                lines[index]=line.replace("T1.dat", "%s.dat" % i)
            elif ("T1.eps" in line):
                #lines[index]=line.replace("T1.eps","NEW_%s.eps" % i)
				lines[index]=line.replace("T1.eps","Figure3_%s.eps" % i[1])
            elif ("T1.png" in line):
                #lines[index]=line.replace("T1.png","NEW_%s.png" % i)
				lines[index]=line.replace("T1.png","Figure3_%s.png" % i[1])
            elif ("T1.jpeg" in line):
                #lines[index]=line.replace("T1.jpeg","NEW_%s.jpeg" % i)
				lines[index]=line.replace("T1.jpeg","Figure3_%s.jpeg" % i[1])
        myfile.writelines(lines)

#Call mcr to draw
draw=["U1", "U3", "U5", "U6"]
mcrdic=dict(U1="case3_velocity1.mcr", U3="case3_velocity3.mcr", U5="case3_velocity5.mcr", U6="case3_velocity6.mcr")
for i in draw:
    args=('tec360', mcrdic[i])
    print args
    subprocess.call(args)

draw=["T1", "T3", "T5", "T6"]
mcrdic=dict(T1="case3_temperature1.mcr", T3="case3_temperature3.mcr", T5="case3_temperature5.mcr", T6="case3_temperature6.mcr")
for i in draw:
    args=('tec360', mcrdic[i])
    print args
    subprocess.call(args)



##clear folder
mcrdic=dict(U1="case3_velocity1.mcr", U3="case3_velocity3.mcr", U5="case3_velocity5.mcr", U6="case3_velocity6.mcr")
lpkdic=dict(U1="case3_velocity1.lpk", U3="case3_velocity3.lpk", U5="case3_velocity5.lpk", U6="case3_velocity6.lpk")
draw=["U1", "U3", "U5", "U6"]

for i in draw:
    #clear lpk file
    ##os.remove(lpkdic[i])
    os.remove(mcrdic[i])
for i in datafile:
    os.remove(i)

draw=["T1", "T3", "T5", "T6"]
mcrdic=dict(T1="case3_temperature1.mcr", T3="case3_temperature3.mcr", T5="case3_temperature5.mcr", T6="case3_temperature6.mcr")
lpkdic=dict(T1="case3_temperature1.lpk", T3="case3_temperature3.lpk", T5="case3_temperature5.lpk", T6="case3_temperature6.lpk")
datafile=["T"+str(i)+".dat" for i in ls ]
for i in draw:
    #clear lpk file
    ##os.remove(lpkdic[i])
    os.remove(mcrdic[i])
for i in datafile:
    os.remove(i)
