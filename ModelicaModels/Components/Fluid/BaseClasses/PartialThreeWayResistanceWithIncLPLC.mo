within ModelicaModels.Components.Fluid.BaseClasses;
partial model PartialThreeWayResistanceWithIncLPLC
  "Flow splitter with partial resistance model at each port with inconstant LPLC"
  extends Buildings.Fluid.Interfaces.LumpedVolumeDeclarations(final
      mSenFac=1);

  Modelica.Fluid.Interfaces.FluidPort_a port_1(
    redeclare package Medium = Medium)
    "Inlet port"
    annotation (Placement(transformation(extent={{-110,-10},{-90,10}})));
  Modelica.Fluid.Interfaces.FluidPort_b port_2(
    redeclare package Medium = Medium)
    "Straight Outlet port"
    annotation (Placement(transformation(extent={{90,-10},{110,10}})));
  Modelica.Fluid.Interfaces.FluidPort_b port_3(
    redeclare package Medium = Medium)
    "Vertical outlet port"
    annotation (Placement(transformation(extent={{-10,-110},{10,-90}})));
  parameter Boolean from_dp = false
    "= true, use m_flow = f(dp) else dp = f(m_flow)"
    annotation (Evaluate=true, Dialog(tab="Advanced"));
  parameter Modelica.SIunits.MassFlowRate mDyn_flow_nominal
    "Nominal mass flow rate for dynamic momentum and energy balance"
    annotation(Dialog(tab="Dynamics", group="Equations",
               enable=not energyDynamics == Modelica.Fluid.Types.Dynamics.SteadyState));

  replaceable Buildings.Fluid.Interfaces.PartialTwoPortInterface res1
    constrainedby Buildings.Fluid.Interfaces.PartialTwoPortInterface(
      redeclare final package Medium = Medium, allowFlowReversal=false)
    "Partial model, to be replaced with a fluid component"
    annotation (Placement(transformation(extent={{-46,-10},{-26,10}})));
  replaceable Buildings.Fluid.Interfaces.PartialTwoPortInterface res2
    constrainedby Buildings.Fluid.Interfaces.PartialTwoPortInterface(
      redeclare final package Medium = Medium, allowFlowReversal=false)
    "Partial model, to be replaced with a fluid component"
    annotation (Placement(transformation(extent={{48,-10},{28,10}})));
  replaceable Buildings.Fluid.Interfaces.PartialTwoPortInterface res3
    constrainedby Buildings.Fluid.Interfaces.PartialTwoPortInterface(
      redeclare final package Medium = Medium, allowFlowReversal=false)
    "Partial model, to be replaced with a fluid component" annotation (
      Placement(transformation(
        origin={0,-42},
        extent={{-10,10},{10,-10}},
        rotation=90)));

public
  Buildings.Fluid.Sensors.Velocity senVel1(
    final tau=0,
    redeclare package Medium = Medium)
    "Upstream velocity sensor"
    annotation (Placement(transformation(extent={{-82,-10},{-62,10}})));
  Buildings.Fluid.Sensors.Velocity senVel2(
    final tau=0,
    redeclare package Medium = Medium)
    "Upstream velocity sensor"
    annotation (Placement(transformation(extent={{64,-10},{84,10}})));
  Buildings.Fluid.Sensors.Velocity senVel3(
    final tau=0,
    redeclare package Medium = Medium)
    "Upstream velocity sensor"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=90,
        origin={0,-72})));
equation

  connect(port_1, senVel1.port_a)
    annotation (Line(points={{-100,0},{-92,0},{-82,0}}, color={0,127,255}));
  connect(senVel1.port_b, res1.port_a)
    annotation (Line(points={{-62,0},{-56,0},{-46,0}}, color={0,127,255}));
  connect(senVel2.port_a, res2.port_a)
    annotation (Line(points={{64,0},{48,0}},        color={0,127,255}));
  connect(senVel2.port_b, port_2)
    annotation (Line(points={{84,0},{100,0}},         color={0,127,255}));
  connect(res3.port_a, senVel3.port_a)
    annotation (Line(points={{0,-52},{0,-57},{0,-62}}, color={0,127,255}));
  connect(senVel3.port_b, port_3)
    annotation (Line(points={{0,-82},{0,-91},{0,-100}}, color={0,127,255}));
  connect(res1.port_b, res2.port_b)
    annotation (Line(points={{-26,0},{1,0},{28,0}}, color={0,127,255}));
  connect(res3.port_b, res2.port_b)
    annotation (Line(points={{0,-32},{0,0},{28,0}}, color={0,127,255}));
end PartialThreeWayResistanceWithIncLPLC;
