within ModelicaModels.Components.Fluid.Example;
model FlowJunction_InconstantLPLC
    "Test model for the three way splitter/mixer model"
    extends Modelica.Icons.Example;
    import Buildings;
   package Medium = Buildings.Media.Air "Medium model";

    Buildings.Fluid.Sources.Boundary_pT bou2(
      redeclare package Medium = Medium,
      T=273.15 + 20,
      p(displayUnit="Pa") = 101325,
      nPorts=1) "Pressure boundary condition"
      annotation (Placement(transformation(extent={{96,22},{76,42}})));

    Modelica.Blocks.Sources.Ramp M(duration=30, height=2.4)
                "Ramp mass flow rate signal"
      annotation (Placement(transformation(extent={{-86,30},{-66,50}})));
    Buildings.Fluid.Sources.MassFlowSource_T boundary(
      use_m_flow_in=true,
      redeclare package Medium = Medium,
      nPorts=1)
      annotation (Placement(transformation(extent={{-46,22},{-26,42}})));
  Junction.PolynomialJunction_Uncertain_addFilter jun(
    redeclare package Medium = Medium,
    A={1,1,1},
    dp_flow_nomial={0,-1,-1},
    m_flow_nominal={2.4,-1.2,-1.2}) "polynomial junction model "
    annotation (Placement(transformation(extent={{16,22},{36,42}})));
    Buildings.Fluid.Sensors.MassFlowRate senMasFlo(redeclare package Medium = Medium)
      annotation (Placement(transformation(extent={{-14,22},{6,42}})));
    Buildings.Fluid.Sensors.MassFlowRate senMasFlo1(redeclare package Medium = Medium)
      annotation (Placement(transformation(extent={{46,22},{66,42}})));
    Buildings.Fluid.Sensors.MassFlowRate senMasFlo2(redeclare package Medium = Medium)
      annotation (Placement(transformation(extent={{6,-38},{-14,-18}})));
    Buildings.Fluid.Sources.MassFlowSource_T boundary1(
      use_m_flow_in=true,
      redeclare package Medium = Medium,
      nPorts=1) annotation (Placement(transformation(extent={{-56,-38},{-36,-18}})));
    Modelica.Blocks.Sources.Ramp M1(duration=30, height=-1.20)
                "Ramp mass flow rate signal"
      annotation (Placement(transformation(extent={{-90,-30},{-70,-10}})));
equation
    connect(boundary.m_flow_in,M. y) annotation (Line(points={{-46,40},{-65,40}},
                             color={0,0,127}));
    connect(senMasFlo.port_a, boundary.ports[1])
      annotation (Line(points={{-14,32},{-26,32}}, color={0,127,255}));
    connect(senMasFlo.port_b, jun.port_1) annotation (Line(points={{6,32},{16,
          32}},                color={0,127,255}));
    connect(senMasFlo1.port_a, jun.port_2)
      annotation (Line(points={{46,32},{36,32}},color={0,127,255}));
    connect(senMasFlo1.port_b, bou2.ports[1])
      annotation (Line(points={{66,32},{76,32}},         color={0,127,255}));
    connect(senMasFlo2.port_a, jun.port_3) annotation (Line(points={{6,-28},{26,
          -28},{26,22}},       color={0,127,255}));
    connect(boundary1.ports[1], senMasFlo2.port_b) annotation (Line(
          points={{-36,-28},{-14,-28}},           color={0,127,255}));
    connect(M1.y, boundary1.m_flow_in) annotation (Line(points={{-69,-20},{-56,
          -20}},                    color={0,0,127}));

end FlowJunction_InconstantLPLC;
