within ModelicaModels.Components.Fluid.Junction;
model PolynomialJunction_Uncertain_addFilter
  "Flow splitter with inconstant LPLC defined as a polynomial function and uncertain factor"
  import Buildings;
    extends BaseClasses.PartialThreeWayResistanceWithIncLPLC(
    mDyn_flow_nominal=sum(abs(m_flow_nominal[:])/3), final from_dp = false,
    redeclare Buildings.Fluid.FixedResistances.PressureDrop
      res1(
      final allowFlowReversal=false,
      from_dp=from_dp,
      final m_flow_nominal=m_flow_nominal[1],
      linearized=linearized,
      homotopyInitialization=homotopyInitialization,
      final dp_nominal=dp_flow_nomial[1],
      final deltaM=0.1),
    redeclare Junction.BranchResistance_UncIncLPLC res2(
      final m_flow_nominal=m_flow_nominal[2],
      final PolyCoef=PolyCoef[1, :],
      final dp_nominal=dp_flow_nomial[2],
      final CorFac=CorFac[1],
      use_inputFilter=use_inputFilter,
      riseTime=riseTime),
    redeclare Junction.BranchResistance_UncIncLPLC res3(
      final m_flow_nominal=m_flow_nominal[3],
      final PolyCoef=PolyCoef[2, :],
      final dp_nominal=dp_flow_nomial[3],
      final CorFac=CorFac[2],
      use_inputFilter=use_inputFilter,
      riseTime=riseTime),
    senVel1(A=A[1], m_flow_nominal=m_flow_nominal[1]),
    senVel2(A=A[2], m_flow_nominal=-m_flow_nominal[2]),
    senVel3(A=A[3], m_flow_nominal=-m_flow_nominal[3]));

  parameter Real PolyCoef[2,:] = [0.4526,-0.9833,0.2801,0.2785;0.6014,-1.1521,1.1244,-0.0537]
                                                                                      "Polynomial coefficients for calculating LPLC 
  of straight branch and vertical branch, the first row is for straight";
  parameter Real CorFac[2] = {1,1} "Correction factor for uncertainty, 1st value is for straigth branch, 2nd is for vertical";
  parameter Modelica.SIunits.Area[3] A "Cross-sectional area for upstream branch, straight branch and vertical branch";
  parameter Modelica.SIunits.MassFlowRate[3] m_flow_nominal
    "Mass flow rate. Each element is for upstream, straight, and vertical branch successively. Negative value for outlet."
    annotation(Dialog(group = "Nominal condition"));
    parameter Modelica.SIunits.PressureDifference[3] dp_flow_nomial(displayUnit="Pa")={0,-1,-1}
    "Pressure drop. Each element is for upstream, straight, and vertical branch successively. Negative value for outlet"
     annotation(Dialog(group = "Nominal condition"));
   parameter Boolean linearized = false
    "= true, use linear relation between m_flow and dp for any flow rate"
    annotation(Dialog(tab="Advanced"));

  parameter Boolean homotopyInitialization = true "= true, use homotopy method"
    annotation(Evaluate=true, Dialog(tab="Advanced"));
  parameter Boolean use_inputFilter=true
    "= true, if opening is filtered with a 2nd order CriticalDamping filter"
    annotation(Dialog(tab="Dynamics", group="Filtered dp"));
  parameter Modelica.SIunits.Time riseTime=120
    "Rise time of the filter (time to reach 99.6 % of an opening step)"
    annotation(Dialog(tab="Dynamics", group="Filtered dp",enable=use_inputFilter));
equation
  connect(senVel2.v, res2.V_down) annotation (Line(points={{74,11},{74,11},{74,26},
          {29.9,26},{29.9,5.1}}, color={0,0,127}));
  connect(senVel3.v, res3.V_down) annotation (Line(points={{11,-72},{28,-72},{28,
          -33.9},{5.1,-33.9}}, color={0,0,127}));

  connect(senVel1.v, res2.V_up) annotation (Line(points={{-72,11},{-72,34},{-72,
          38},{45.9,38},{45.9,5.1}}, color={0,0,127}));
  connect(res3.V_up, res2.V_up) annotation (Line(points={{5.1,-49.9},{18,-49.9},
          {18,38},{45.9,38},{45.9,5.1}}, color={0,0,127}));
  annotation (Icon(graphics={
        Polygon(
          points={{-100,-46},{-32,-40},{-32,-100},{30,-100},{30,-36},{100,-30},
              {100,38},{-100,52},{-100,-46}},
          lineColor={0,0,0},
          fillColor={175,175,175},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-100,-34},{-18,-28},{-18,-100},{18,-100},{18,-26},{100,-20},
              {100,22},{-100,38},{-100,-34}},
          lineColor={0,0,0},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={0,128,255}),
        Ellipse(
          visible=not energyDynamics == Modelica.Fluid.Types.Dynamics.SteadyState,
          extent={{-38,36},{40,-40}},
          lineColor={0,0,127},
          fillColor={0,0,127},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-88,-54},{90,-92}},
          lineColor={0,0,0},
          fillColor={0,0,127},
          fillPattern=FillPattern.Solid,
          textString="Polynomial LPLC")}));
end PolynomialJunction_Uncertain_addFilter;
