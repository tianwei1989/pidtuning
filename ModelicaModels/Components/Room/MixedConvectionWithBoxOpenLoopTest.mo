within ModelicaModels.Components.Room;
model MixedConvectionWithBoxOpenLoopTest
  "Open loop test of mixed convection with a Box case"
  extends Modelica.Icons.Example;
  extends Room.MixedConvectionWithBoxBase
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));

equation

  annotation (Diagram(coordinateSystem(extent={{-100,-180},{240,100}})), Icon(
        coordinateSystem(extent={{-100,-180},{240,100}})));
end MixedConvectionWithBoxOpenLoopTest;
