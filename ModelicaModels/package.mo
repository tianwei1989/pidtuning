within ;
package ModelicaModels "ProjectModels"
  annotation (uses(Modelica(version="3.2.2"),Buildings(version="5.0.1")));
end ModelicaModels;
