# -*- coding: utf-8 -*-
"""
Created on Tue Mar 20 12:22:41 2018

@author: yunyangye
"""
import pyDOE as doe
import shutil as st
from tempfile import mkstemp
from os import remove, close, system
import information as inf
import readEnergyUse as rene
import replace as rp
import time
import multiprocessing as mp
import math
import xlsxwriter

######################################################
#lhs sampling
######################################################
#mini is the list of attributes' minimum values (uniform)
#maxi is the list of attributes' maximum values (uniform)
#num is the number of samples
def lhsSample(mini,maxi,num):
    att = len(mini)
    #uniform distribution
    design = doe.lhs(att, samples=num, criterion='center')
    
    lhs_result = []
    for row in design:
        temp = []
        for ind,val in enumerate(row):
            temp.append(mini[ind] + val * (maxi[ind] - mini[ind]))
        lhs_result.append(temp)    
        
    return lhs_result

######################################################
#get the value of text
######################################################
#text is the name of item
#page is content of file
def getValue(text,page):
    n = len(text)
    #find text
    k = 0
    for check in range(0,2):
        for index,x in enumerate(page):
            if page[index:index+n] == text:
                k = index+n
            
            #get the first char's index of information
            ind = []
            for i in range(0,10):
                ind.append(str(i))
                
        first_char = 0
        for i in range(k+1, k+1000):
            if page[i:i+8] == 'Autosize':
                first_char = -1
                break
            elif page[i] in ind:
                first_char = i
                break
        
        if first_char != -1:    
            #get the last char's index of information
            last_char = 0
            for i in range(first_char, first_char+1000):
                if page[i] == ';':
                    last_char = i-1
                    break
            break

    #get the existing value of text
    return page[first_char:last_char+1]

######################################################
#replace the contents of a file from pattern to subst
######################################################
#file_path: the file path
#pattern: the old information needs to be changed
#subst: the new information needs to be added
def replace(file_path, pattern, subst):
    #create temp file
    fh, abs_path = mkstemp()
    
    #change weather file name or seed file name
    with open(abs_path, 'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))

    close(fh)
    #remove original file from the file path
    remove(file_path)
    #move new file into the file path
    st.move(abs_path,file_path)

######################################################
#run models
######################################################
#baseline is the climate zone information
#measure is the measures and values are used in each case (two-dimension list): If no measure, measure = 'NO'
#num_model is the NO. of the model
def runModel(baseline,measure,num_model,output):
    #copy source folder and rename the new name
    st.copytree('./source','./'+str(num_model))
    
    #copy the .osm file into the new folder
    st.copyfile('./Pre/CZ' + baseline + '.osm', './'+str(num_model)+'/files/CZ' + baseline + '.osm')

    #change the weather_file and seed_file in compact.osw
    weather_file = baseline + '.epw'
    seed_file = 'CZ' + baseline + '.osm'
    
    with open('./'+str(num_model)+'/compact.osw', "r") as f:
        page = f.read()
    
    old_weather = inf.getValue1('weather_file',page)
    rp.replace('./'+str(num_model)+'/compact.osw',old_weather,weather_file,'NO','NO')

    old_seed = inf.getValue1('seed_file',page)
    rp.replace('./'+str(num_model)+'/compact.osw',old_seed,seed_file,'NO','NO')

    with open('./'+str(num_model)+'/files/CZ' + baseline + '.osm', "r") as f:
        page1 = f.read()
    
    #change the cooling and heating capacity
    data = getValue('! Chilled Water Outlet Node Name',page1)
    data_float = float(data)
    data_new1 = (measure[0]+1)*data_float
    replace('./'+str(num_model)+'/files/CZ' + baseline + '.osm',data+';',str(data_new1)+';')
    
    data = getValue('! Hot Water Outlet Node Name',page1)
    data_float = float(data)
    data_new2 =(measure[1]+1)*data_float
    replace('./'+str(num_model)+'/files/CZ' + baseline + '.osm',data+';',str(data_new2)+';')
    
    if len(measure) > 2:
        #change the values in the existing sebi.osw based on measure
        inf.measInfo(str(num_model),measure[2:])

    #run the models
    #'/usr/bin/openstudio': path of the software
    system("'/usr/bin/openstudio' run -w './"+str(num_model)+"/compact.osw'")
    
    #get the results
    #output data
    output_data = [baseline,measure[0],measure[1]]
    #record the models
    if len(measure) > 2:
        for row in measure[2:]:
            output_data.append(str(row[2]))

    #source eui and site eui
    total_site, total_source, total_area = rene.readEnergyUse(num_model)
    output_data.append(total_source/total_area*1000.0/11.35653)
    output_data.append(total_site/total_area*1000.0/11.35653)
    
    #check whether errors are in the modelworksheet.write('A'+str(line_ods_1), row[0])
    if total_area < 0:
        text = ''
        for x in baseline:
            text = text + str(x)
        text_file = open("error.txt", "a")
        text_file.write(text+'\n')
        text_file.close()
    
    output.put(output_data)
    
    #delete the folder
    st.rmtree('./'+str(num_model))

######################################################
#run models in parallel
######################################################
#num is the number of the models that we want to get from the lhs
def parallelSimu(num):
    #record the start time
    start = time.time()
    
    #name the baseline models
    Baseline = ['1A']
    
    #Measure is the measures and values are used in all cases (three-dimension list)
    #set the min and max values for each measure
    #Heat-Cap,Cool-Cap,lpd,pd,epd,R-val-wall,R-val-roof,U-fac-window,SHGC,WWR,overhang,F2FH,gas
    mini = [-1,-1,15.5,0.03,-22.22,1.76,7.99,0.925,0.486,0.05,0,9,0.34]
    maxi = [1,1,23.36,0.31,6.17,2.16,9.77,1.13,0.594,0.38,0.8,15,0.52]
    
    value_model = lhsSample(mini,maxi,num)
    
    Measure = []
    for row in value_model:
        temp = [row[0],row[1]]
        temp.append(['ReplaceLightsInSpaceTypeWithLPD','lpd',row[2]])
        temp.append(['ReplacePeopleDensity','pd',row[3]])
        temp.append(['ChangeElectricEquipmentLoadsByPercentage','elecequip_power_change_percent',row[4]])
        temp.append(['ChangeInsulationRValueForExteriorWalls','r_value',row[5]])
        temp.append(['ChangeInsulationRValueForRoofs','r_value_roof',row[6]])
        temp.append(['ChangeUValueandSHGCOfWindows','window_u_value_ip',row[7]])
        temp.append(['ChangeUValueandSHGCOfWindows','window_shgc',row[8]])
        temp.append(['ResizeExistingWindowsToMatchAGivenWWR','wwr',row[9]])
        temp.append(['AddOverhangsByProjectionFactor','projection_factor',row[10]])
        temp.append(['ChangeFloorToFloorHeight','floor_to_floor_height_ip',row[11]])
        temp.append(['ChangeEnergyUseForKitchen','gas',row[12]])
        Measure.append(temp)
    
    #multi-processing
    output = mp.Queue()
    processes = [mp.Process(target=runModel, args = (Baseline[i],Measure[j],i*len(Measure)+(j+1),output)) for i in range(len(Baseline)) for j in range(len(Measure))]
    
    #count the number of cpu
    cpu = mp.cpu_count()#record the results including inputs and outputs
    print cpu
    
    model_results = []
    
    run_times = math.floor(len(processes)/cpu)
    if run_times > 0:
        for i in range(int(run_times)):
            for p in processes[i*int(cpu):(i+1)*int(cpu)]:
                p.start()
            
            for p in processes[i*int(cpu):(i+1)*int(cpu)]:
                p.join()
    
            #get the outputs
            temp = [output.get() for p in processes[i*int(cpu):(i+1)*int(cpu)]]
            
            for x in temp:
                model_results.append(x)
    
    for p in processes[int(run_times)*int(cpu):len(processes)]:
        p.start()
            
    for p in processes[int(run_times)*int(cpu):len(processes)]:
        p.join()    
        
    #get the outputs
    temp = [output.get() for p in processes[int(run_times)*int(cpu):len(processes)]]
    for x in temp:
        model_results.append(x)
            
    #record the end time
    end = time.time()
    
    return model_results,end-start

model_results,total_time = parallelSimu(2000)

print model_results
print total_time

#create a new excel file
workbook = xlsxwriter.Workbook('./energy_use.ods')
worksheet = workbook.add_worksheet()

column = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T']

for ind1,row in enumerate(model_results):
    for ind2,val in enumerate(row):
        worksheet.write(str(column[ind2])+str(ind1+1), val)

#close excel file
workbook.close()
